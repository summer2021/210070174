package org.apache.iotdb.db.exception.layoutoptimize;

public class SampleRateNoExistsException extends LayoutOptimizeException {
  public SampleRateNoExistsException(String message) {
    super(message);
  }
}
